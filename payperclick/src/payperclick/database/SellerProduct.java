package payperclick.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="Seller_Product")
public class SellerProduct {
	@NotNull
	@Id
	private String SellerId;
	private String SUPC;
	private boolean promoted;
	private int Counter;
	public String getSellerId() {
		return SellerId;
	}
	public void setSellerId(String sellerId) {
		SellerId = sellerId;
	}
	public String getSUPC() {
		return SUPC;
	}
	public void setSUPC(String sUPC) {
		SUPC = sUPC;
	}
	public boolean isPromoted() {
		return promoted;
	}
	public void setPromoted(boolean promoted) {
		this.promoted = promoted;
	}
	public int getCounter() {
		return Counter;
	}
	public void setCounter(int counter) {
		Counter = counter;
	}
	
}
