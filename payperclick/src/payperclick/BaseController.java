package payperclick;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BaseController {
	@RequestMapping("/search")
	public String firstPage(ModelMap m)
	{
		Search search = new Search();
		m.put("searchForm", search);
		return "search";
	}
	@RequestMapping(value="/query", method = RequestMethod.POST)
	public ModelAndView method(@ModelAttribute("searchForm") Search search){
//		ModelAndView modelAndView = new ModelAndView("NewFile");
//		return modelAndView;
		search.setUrl();
		search.setOurUrl(search.query);
		return new ModelAndView("search");
	}
	
	@RequestMapping(value="/ads/{query}")
	public ModelAndView showAds(@PathVariable("query") String s){
		
		ModelAndView modelAndView = new ModelAndView("ads");
		return modelAndView;
	}
}
